# Libraries
import matplotlib.pyplot as plt
import pandas as pd
from math import pi

# Set data
df = pd.DataFrame({
    'group': ['A', 'B', 'C', 'D'],
    'var1': [38, 1.5, 30, 4],
    'var2': [29, 10, 9, 34],
    'var3': [8, 39, 23, 24],
    'var4': [7, 31, 33, 14],
    'var5': [28, 15, 32, 14]
})

#list(df) takes the header of the datagrame. With [1:] it is carving out the 'group' element
categories=list(df)[1:]
#number of categories that we have
N = len(categories)

# What will be the angle of each axis in the plot? (we divide the plot / number of variable)
angles = [n / float(N) * 2 * pi for n in range(N)] # now we have our angles
#but we also need to add to the last angles, since we need to close the circle
angles += angles[:1] # with [:1] you are taking the first element. with [:2] you take the first two etc.

#we take the first column of the dataframe
values = df.loc[0]
#we remove the header
values = values.drop('group')
#we go to a list
values = values.values
values = values.flatten()
#now we go to a list (before we wew an array object)
values = values.tolist()
# But we need to repeat the first value to close the circular graph:
values += values[:1]

#create the radar
ax = plt.subplot(111, polar=True)

#set the angles, set the names of the angles, their color and their sizes
plt.xticks(angles[:-1], categories, color='grey', size=10)

ax.plot(angles, values, linewidth=1, linestyle='solid')
ax.fill(angles, values, 'b', alpha=0.1)

plt.show()
