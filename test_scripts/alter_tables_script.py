import psycopg2 as psy
from properties.properties import *


connection = psy.connect(
            "host=" + host +
            " dbname=" + database_name +
            " user=" + user_name +
            " password=" + password)

cur = connection.cursor()

sql = 'alter table contributor add column anonymous_id  int default 0'

cur.execute(sql)

print('done')


