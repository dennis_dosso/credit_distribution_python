import numpy as np
import seaborn as sb
import matplotlib.pyplot as plt

"""A demo to work with seaborn, a simple library to deal with hitmaps. 
The tutorial used here eas taken from 
https://likegeeks.com/seaborn-heatmap-tutorial/
"""

data = np.random.rand(4, 6)

sb.set(font_scale=0.8)
heat_map = sb.heatmap(data, yticklabels=False, cbar_kws = {'orientation' : 'horizontal'})
plt.xlabel("Values on X axis")
plt.ylabel("Values on Y axis")
plt.show()