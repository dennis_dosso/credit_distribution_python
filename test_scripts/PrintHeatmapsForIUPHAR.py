


#these two imports are redundant, I was just learning a little bit
#here you are importing one class from the module GenerateHeatmapsFromTables
# from modules.GenerateHeatmapsFromTables import HeatMapFromDBGenerator

# here you are importing the name of the module. If you want to use some function you need to call it
#using the name of the module. e.g. GenerateHeatmapsFromTables.function_name
from modules import GenerateHeatmapsFromTables

#create the generator
generator = GenerateHeatmapsFromTables.HeatMapFromDBGenerator()
#print the tables that were used by the queries found for IUPHAR
generator.print_one_table_from_DB("family")
generator.print_one_table_from_DB("grac_family_text")
generator.print_one_table_from_DB("receptor2family")
generator.print_one_table_from_DB("receptor_basic")
generator.print_one_table_from_DB("object")
generator.print_one_table_from_DB("grac_further_reading")
generator.print_one_table_from_DB("reference")
generator.print_one_table_from_DB("contributor2family")
generator.print_one_table_from_DB("contributor")