from modules.GenerateHeatmapsFromTables import HeatMapFromDBGenerator

#create the generator
generator = HeatMapFromDBGenerator()
#print two maps for test
generator.print_one_table_from_DB("agencies")
generator.print_one_table_from_DB("externaltours")