import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from properties.properties import *

# read the csv file
table = pd.read_csv(authors_map_output_file)
# isolate the two columns
citation_counts = table[['citation_count']].values
citation_counts = np.atleast_2d(citation_counts).T
citation_counts = citation_counts[0]

credits_ = table[['credit']]
credits_ = credits_.values
credits_ = np.atleast_2d(credits_).T
credits_ = credits_[0]

bins = np.arange(1, len(citation_counts)+1)

fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel('authors')
ax1.set_ylabel('citations')
ax1.plot(bins, citation_counts, color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('credit', color=color)  # we already handled the x-label with ax1
ax2.plot(bins, credits_, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.show()

