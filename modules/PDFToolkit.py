import PyPDF2 as pyPdf
from PyPDF2 import utils


class PDFToolkit:
    """ Contains methods that I found useful when dealing with PDFs"""

    @staticmethod
    def read_all_urls_in_pdf(pdf_path):
        """
        Retruns a list of urls, which are the ones extracted from the PDF file
        :param pdf_path: path of the PDF file that we are reading
        :return: a list with all the urls that are contained in the PDF file
        """
        # open the file as PDF
        PDFFile = open(pdf_path, 'rb')
        try:
            PDF = pyPdf.PdfFileReader(PDFFile)
        except Exception as e:
            print(e.__class__)
            print("problems with file " + pdf_path)
            return None

        # take the number of pages
        pages = PDF.getNumPages()
        # look for URIs
        key = '/Annots'
        url_list = []

        for page in range(pages):  # for each page
            # take the single page
            pageSliced = PDF.getPage(page)
            # get the page has object
            pageObject = pageSliced.getObject()
            # get the list of all the keys of this page (they describe what we can find inside of it)
            keys = pageObject.keys()
            if key in keys:
                # get this annotation (a hyperlink), which will be a list of hyperlinks
                ann = pageObject.get(key)
                ann = ann.getObject()
                for a in ann:  # for every object in the list of urls objects
                    u = a.getObject()
                    # u is a dictionary. First, get '/A'
                    if u.get('/A') is None:  # sanity check
                        continue
                    dict1 = u['/A']
                    # get the url
                    if dict1.get('/URI') is None:  # another sanity check
                        continue
                    url = dict1['/URI']
                    # append the damn thing
                    url_list.append(url)
        return url_list
