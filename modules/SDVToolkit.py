import os as os

import pandas as pd
from sdv import Metadata, SDV

from modules.PropertiesMethods import *

# where I put the 'property' file, where you can put parameters so you can easily change them
filename = '/properties/properties.json'


class SDVToolkit:
    name = 'SchemaDefiner'

    def __init__(self):
        """In this class you can find useful methods based on the SDV library"""
        # define a data structure with informations about the paths
        self.properties_data = read_property_file(filename)

    def buildAndPrintSDV(self):
        """Reads from a directory containing csv which represents tables these tables and creates
        the metadata which represent the schema of the database.
        You need to have these csv file to create these metadata.
        These csv files will be used to train the sdv engine, so the more data you are going to have
        in your csv, the more various the dataq will appear.


        """
        # take the path where the csv representing tables are stored
        path = self.properties_data['csv_training_data_directory']
        files = os.listdir(path)

        # build a Metadata object
        metadata = Metadata()
        # an empty dictionary containing the tables that we are going to read from disk
        tables = {}

        # now, for every file, read it and add it to the metadata
        for file in files:
            completePath = path + "/" + file
            # read the table from csv
            table = pd.read_csv(path)
            # add this table to the files dictionary
            tables[file] = table
            # add this table to the schema
            self.addTableToMetadata(metadata, table, file)

        # if we are here, we read all the tables and we added them to the metadata
        # now we create an SVD machine
        sdv = SDV()
        # perform the fitting
        sdv.fit(metadata, tables)
        # save the model for future use
        sdv.save(self.properties_data["sdv_output_file"])

    def sampleElementsFromSDV(self, sdvPath: str, numOfSamples: int) -> object:
        """Given the path of a trained sdv, produces a file with the indicated elements of samples.
        
        :param sdvPath the path of the SDV machine pre-trained
        :param numOfSamples the number of samples that this sdv needs to produce. This number refers to the number
        of tuples generated for the primary tables in the sdv. The children tables will probably have more lines, so
        be careful with what you wish.

        :return samples a dictionary of panda dataframes, every one of them represents a table
        """
        # read the sdv from disk
        sdv = SDV.load(sdvPath)
        samples: object = sdv.sample_all(numOfSamples)
        # the result is a list of samples
        return samples

    def printSamplesToCSV(self, samples: object, outputDir: object) -> object:
        """ Given a dictionary of DataFrame, it prints them in the corresponding directory

        @:param outputDir where to save the csv files. One file for each table.
        """
        for table in samples:
            # for every table
            path = outputDir + "/" + table + ".csv"
            # now that you have the table, you can save it to disk
            samples[table].to_csv(path)

    def addTableToMetadata(self, metadata: Metadata, table: object, name: str):
        """ takes the metadata to be populated, a table which is a Panda DataFrame and the name of the table,
        inserts the table in the schema (metadata) with that name. Since you have to specify
        keys and external keys, we hardoce here the structure of the schema. In the future we may need to
        find, if possible, a better way to explain this."""

        # todo finish this method depending on your requirements
        if (name == 'users'):
            metadata.add_table(name, table, primary_key='user_id')
        elif (name == ''):
            # todo aggiungi le altre tabelle e relazioni al database
            # parent is used to indicate the parent table, foreign_key is the key, in this table,
            # which refers to the primary key in the other table.
            metadata.add_table(name, table, primary_key='user_id', parent='sessions', foreign_key='session_id')
        else:
            print(f'no tables with name {name} programmed')

    def defineAndPrintMetadataDemo(self) -> object:
        """Class which defines the schema that is then defined and prints it
        You have to hardcode here your schemata for now. """

        # create empty metadata
        metadata = Metadata()

        self.defineUsersTable(metadata)

        # save the metadata
        metadata.to_json("/Users/dennisdosso/Documents/Python/credit/metadata/test_metadata.json")

    def defineUsersTable(self, metadata: Metadata):
        """

        :rtype: object
        """
        # this method defines the users table. You should define one such method for every table that you want to define
        # first define a simple table with your schema
        # you also need at least some data (only 1 tuple)
        data = {'Name': ['Jai'],
                'Age': [27],
                'Address': ['Delhi'],
                'Qualification': ['Msc']}
        # convert it into Panda DataFrame
        df = pd.DataFrame(data)
        metadata.add_table("users", df, primary_key="Name")
        # example (for the future) of how to include
        # metadata.add_table('sessions', data=tables['sessions'], primary_key='session_id', parent='users',
        #                    foreign_key='user_id')


def main():
    """ Test main """
    print("Starting the creation of a SDV schema that will be written in the disk for future reference")

    # create the object
    execution = SDVToolkit()
    # create the sdv engine and save it
    execution.buildAndPrintSDV()

    # create the synthetic database
    samples = execution.sampleElementsFromSDV(
        execution.properties_data["sdv_output_file"],
        int(execution.properties_data["number_of_samples"]))

    # print the samples in csv
    execution.printSamplesToCSV(samples, execution.properties_data["csv_data_directory"])


SDVToolkit.main()
