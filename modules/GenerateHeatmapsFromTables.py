import numpy as np
import seaborn as sb
import matplotlib.pyplot as plt
from properties.properties import *
import psycopg2 as psy
import pandas as pd

""" Script to generate heatmaps from tables in a database
NB!!!: if you are planning to use the csv, consider to 
use the order by version of the query SQL_EXTRACT_COLUMN 
"""

SQL_EXTRACT_COLUMN = "select %s from %s"
# SQL_EXTRACT_COLUMN = "select %s from %s order by %s"


class HeatMapFromDBGenerator:

    def __init__(self):
        """This class contains method that enable to create heat maps from the credit information
        contained in database tables.
        The connection to the database is dealt with the information contained in the properties/properties.py
        file.
        """
        # a connection to database
        self.connection = psy.connect(
            "host=" + host + " dbname=" + database_name + " user=" + user_name + " password=" +
            password)
        self.cur = self.connection.cursor()

    def extractOneColumnFromDB(self, table: str, column_name: str):
        """
        Queries the table extracting the column that we want. It also prints this column
        :param table: table from which tou want to extract one column
        :param column: name of the column/attribute that you want to extract
        :return: a list with the values of your column
        """
        # prepare and execute the query
        query = SQL_EXTRACT_COLUMN % (column_name, table)
        self.cur.execute(query)
        column = []
        results = self.cur.fetchall()
        for tuple in results:
            column.append(tuple[0])

        # ----- this the code to print the table ----- #
        # path of the file were we are saving this list
        output_path = output_csv_directory + "/" + table + "_" + column_name + suffix + ".csv"
        # use pandas which has nice methods to write csv
        a = pd.DataFrame(column)
        # write without indexes, we do not need them, we only want the data
        a.to_csv(output_path, index=False, header=False)

        return column

    def print_one_table_from_DB(self, table_name):
        """
        Given the name of a table in the DB, prints the heatmap
        of its provenance columns.
        :param table_name:
        :return:
        """
        lineage_values = self.extractOneColumnFromDB(table_name, "lineage_credit")
        why_values = self.extractOneColumnFromDB(table_name, "why_credit")
        how_values = self.extractOneColumnFromDB(table_name, "how_credit")
        data_ = []
        size = len(lineage_values)
        for i in range(0, size):
            lin = [lineage_values[i], why_values[i], how_values[i]]
            data_.append(lin)

        sb.set(font_scale=0.8)
        heat_map = sb.heatmap(data_,
                              cmap="PuRd",
                              yticklabels=False,
                              cbar_kws={'orientation': 'horizontal'})
        plt.xlabel(table_name)
        plt.ylabel("tuples")

        N = 3
        ind = [0.5, 1.5, 2.5]
        plt.xticks(ind, ('lineage', 'why', 'how'))

        output_file = output_hitmaps_directory + "/" + table_name + ".png"
        plt.savefig(output_file)

        plt.show()
        plt.close()

    def print_one_column_from_file(self, file_path,
                                   table_name='default_name',
                                   columns_=5,
                                   suffix_='',
                                   file_format='pdf',
                                   figure_width=10,
                                   figure_height=30,
                                   column_index=0
                                   ):
        """
        Given a file containing values, this method creates a heatmap from only one column
        it.
        :param file_path:
        :param table_name:
        :param columns_:
        :param suffix_:
        :param file_format:
        :param figure_width:
        :param figure_height:
        :return:
        """
        from modules.rum import extract_column_from_csv_file
        from math import floor
        # get the credit values from the file
        values = extract_column_from_csv_file(file_path, column_index)
        # compute the number of tuples that we include in each column
        size = len(values)
        tuples_in_one_column = size / columns_
        tuples_in_one_column = floor(tuples_in_one_column)

        # now print this data
        self.print_one_column(tuples_in_one_column, columns_, values, figure_width, figure_height,
                              table_name, suffix_, file_format)

    @staticmethod
    def print_one_column(tuples_in_one_column,
                         columns_,
                         values,
                         figure_width=10,
                         figure_height=30,
                         table_name='',
                         suffix_='',
                         file_format='pdf',
                         output_hitmaps_directory_=output_hitmaps_directory):
        """
        Use this method to print one array of values in a heatmap. The vector
        is divided to fit in more columns one next the other.

        :param tuples_in_one_column: number of elements in one column
        :param columns_: number of columns you want to show
        :param values: the array you want to print
        :param figure_width: the width of the figure
        :param figure_height: the heihght of the figure
        :param table_name: the name you want to put in the x axis
        :param suffix_: something to write in the filename
        :param file_format: format of the file
        :return:
        """
        matrix = []
        # first, initialize the matrix with a set of empty rows
        for j in range(0, tuples_in_one_column):
            matrix.append([])

        # for each column
        for j in range(0, columns_):
            column = []
            # populate this column
            for i in range(0, tuples_in_one_column):
                dat = values[i + (j * tuples_in_one_column)]
                column.append(dat)
            # transform column in an array
            column = np.array(column)
            # add this column to the matrix
            # the .T operation is used to obtain the transpose of the column, which originally is a row
            matrix = np.hstack((matrix, np.atleast_2d(column).T))

        # compute the heatmap
        sb.set(font_scale=.7)

        # dimensions, first width, second height
        fig_dims = (figure_width, figure_height)
        fig, ax_ = plt.subplots(figsize=fig_dims)
        heat_map = sb.heatmap(matrix,
                              cmap="PuRd",
                              yticklabels=False,
                              cbar_kws={'orientation': 'horizontal'},
                              ax=ax_)
        plt.xlabel(table_name)
        # plt.ylabel("tuples")

        # prepare the labels to put in the x axis
        x_labels = []
        ind = []
        for i in range(0, columns_):
            x_labels.append(str(i * tuples_in_one_column + 1) + "-" + str(tuples_in_one_column * (i + 1)))
            ind.append(i + 0.5)

        plt.xticks(ind, x_labels)

        output_file = output_hitmaps_directory_ + "/" + table_name + suffix_ + "." + file_format
        plt.savefig(output_file)

        plt.show()
        plt.close()

    @staticmethod
    def print_one_column_from_csv(tuples_in_one_column,
                                  columns_,
                                  values,
                                  figure_width=10,
                                  figure_height=30,
                                  table_name='',
                                  suffix_='',
                                  file_format='pdf',
                                  output_hitmaps_directory_=output_hitmaps_directory,
                                  vmax_=1.5):
        """
        Use this method to print one array of values in a heatmap. The vector
        is divided to fit in more columns one next the other.

        :param tuples_in_one_column: number of elements in one column
        :param columns_: number of columns you want to show
        :param values: the array you want to print
        :param figure_width: the width of the figure
        :param figure_height: the heihght of the figure
        :param table_name: the name you want to put in the x axis
        :param suffix_: something to write in the filename
        :param file_format: format of the file
        :return:
        """
        matrix = []
        # first, initialize the matrix with a set of empty rows
        for j in range(0, tuples_in_one_column):
            matrix.append([])

        # for each column
        for j in range(0, columns_):
            column = []
            # populate this column
            for i in range(0, tuples_in_one_column):
                dat = values[i + (j * tuples_in_one_column)]
                column.append(dat)
            # transform column in an array
            column = np.array(column)
            # add this column to the matrix
            # the .T operation is used to obtain the transpose of the column, which originally is a row
            matrix = np.hstack((matrix, np.atleast_2d(column).T))

        # compute the heatmap
        sb.set(font_scale=.7)

        # dimensions, first width, second height
        fig_dims = (figure_width, figure_height)
        fig, ax_ = plt.subplots(figsize=fig_dims)
        heat_map = sb.heatmap(matrix,
                              cmap="PuRd",
                              yticklabels=False,
                              xticklabels=False,
                              cbar_kws={'orientation': 'horizontal'},
                              ax=ax_,
                              vmin=0,
                              vmax=vmax_)
        # plt.xlabel(table_name)
        # plt.ylabel("tuples")

        # prepare the labels to put in the x axis
        # x_labels = []
        # ind = []
        # for i in range(0, columns_):
        #     x_labels.append(str(i * tuples_in_one_column + 1) + "-" + str(tuples_in_one_column * (i + 1)))
        #     ind.append(i + 0.5)
        #
        # plt.xticks(ind, x_labels)

        output_file = output_hitmaps_directory_ + "/" + table_name + suffix_ + "." + file_format
        plt.savefig(output_file)

        plt.show()
        plt.close()

    def print_one_table_one_column_from_DB(self, table_name,
                                           column_name="lineage_credit",
                                           columns_=5,
                                           suffix_='',
                                           file_format='pdf',
                                           figure_width=10,
                                           figure_height=30):
        """
        Given the name of a table in the DB, prints the heatmap
        of its provenance columns. It also writes the table in a csv file.

        :param table_name: name of the table in the database that is used by the method
        :param figure_height:
        :param figure_width:
        :param suffix_: string to add to the filename if you want to add something, default is the empty string
        :param columns_: number of columns you want to see in the picture:
        :param column_name the name of the column you want to print
        :param file_format the format of the file. Default to png
        :return:
        """
        from math import floor
        lineage_values = self.extractOneColumnFromDB(table_name, column_name)
        data_ = []
        size = len(lineage_values)

        # how many tuples we present in only one column, six is a magic number
        if columns_ == None:
            columns_ = columns_in_hitmap

        tuples_in_one_column = size / columns_
        tuples_in_one_column = floor(tuples_in_one_column)

        lost_tuples = size - (tuples_in_one_column * columns_)
        print("lost tuples: " + str(lost_tuples))

        # now print this data
        self.print_one_column(tuples_in_one_column, columns_, lineage_values, figure_width, figure_height,
                              table_name, suffix_, file_format)


if __name__ == '__main__':
    print("hello there")
