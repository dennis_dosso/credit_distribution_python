import json


# contains functions that can be useful with properties-like files


def read_property_file(path: str) -> object:
    """Reads a json file (only read)
    and returns the dictionary obtained from that file"""
    with open(path, 'r') as f:
        properties: object = json.load(f)
        return properties
