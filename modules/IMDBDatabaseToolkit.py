import os

from modules.PropertiesMethods import read_property_file
import psycopg2 as psy
import csv

SQL_INSERT = "INSERT INTO %s.name (" \
             "nconst, primaryname, birthyear, deathyear)" \
             " VALUES ('%s', '%s', %s, %s)"

SQL_INSERT_PROFESSION = "INSERT INTO %s.has_profession( " \
                        "person_id, profession) VALUES ('%s', '%s');"

SQL_INSERT_TITLE = "INSERT INTO %s.known_for_title(person_id, title_id) VALUES ('%s', '%s');"

SQL_INSERT_TITLE_ = "INSERT INTO %s.title( " \
                    "tconst, " \
                    "primarytitle," \
                    " year, " \
                    "runtimeminutes," \
                    " author, " \
                    "title_type) " \
                    "VALUES ('%s', '%s', %s, %s, '%s', '%s');"

SQL_INSERT_TITLE_GENRE = "INSERT INTO %s.has_genre( " \
                         "title_id, " \
                         "genre) " \
                         "VALUES ('%s', '%s');"


class DatabaseToolkit:
    """Class which contains methods to read from csv files, create tables in the database and upload them
    Refer to the property file in the filename string to all the variables used in this class"""

    def __init__(self):
        # where I put the 'property' file, where you can put parameters so you can easily change them
        filename = '/properties/properties.json'
        # read the properties and include all of them
        self.properties_data = read_property_file(filename)
        # import informations to connect to the database
        self.host = self.properties_data["host"]
        self.database_name = self.properties_data["database_name"]
        self.user_name = self.properties_data["user_name"]
        self.password = self.properties_data["password"]
        # connect the database
        self.connection = psy.connect(
            "host=" + self.host + " dbname=" + self.database_name + " user=" + self.user_name + " password=" +
            self.password)
        self.cur = self.connection.cursor()

    def createTables(self):
        """create all the specified tables
        -----
        This method executes all the queries in the <b>creation_queries<b> property, which is a list.
        These queries should all be create table.
        """
        # todo here you can add all the tables that you need. Make sure that you added them in the property file
        # list of queries
        queries = self.properties_data["creation_queries"]
        # generate all your tables
        for query in queries:
            self.cur.execute(query)

    def insert_data_in_database(self):
        """As of now, this method is useless, you should uopdate it"""
        # directory with all the csv data file
        directory = self.properties_data["csv_data_directory"]
        # read all the files in the directory
        files = os.listdir(directory)
        # for every file, perform an insertion
        for file in files:
            file_name = os.path.basename(file)
            with open(file, 'r') as file:
                # open the file in read only mode
                reader = csv.reader(file)
                # skip the first line of the file
                next(reader)
                for row in reader:
                    # for every row in the file
                    self.deal_with_one_row(row, file_name)
                    # todo deal with the row

        # when you are done commit
        self.connection.commit()

    def deal_with_one_row(self, row: str, file_name: str):
        """given one row of one csv file, recognizes the row and inserts it in the database using the corresponding
        table using the correct string provided in the properties file

        :param row a csv row to insert in the database
        :param file_name the key in the insertion_queries dictionary in the property file of the insertion query that
        we want to use. If you follow the current notation, the name of each csv file should be the key of this query.
        Thus in the dictionary you should have something like:
        "insertion_queries" : {file_name : insertion query }

        """
        # todo make sure that in this way you actually get the right element
        insertion_query: str = self.properties_data["insertion_queries"][file_name]
        self.cur.execute(insertion_query, row)

    def shutDown(self):
        """shuts down things such as the connection to the database and others.
        """
        self.cur.close()
        self.connection.close()

    # ---------------------------------------------------------------------------------------------------------------------

    # ------------------------------ IMDB ------------------------------
    def insert_imdb_files(self):
        """Use this method to read tsv imdb files and insert them in
        the corresponding database. The directory where the tsv files are stored
        is specified in the property <b>imdb_directory</b>.
        """
        # first, read the files
        directory = self.properties_data["imdb_directory"]
        files = os.listdir(directory)  # take the names of the files in the directory
        for file in files:
            # for every tsv file
            file_path = directory + "/" + os.path.basename(file)
            self.deal_with_one_imdb_file(file, file_path, directory)

    def deal_with_one_imdb_file(self, file_name: str, file_path: str, directory: str):
        """Imports one IMDB tsv file into the database. This is not 'object oriented' as method,
        a little bit of spaghetti code, but since there are only these two cases please forgive me
        (por mi vida loca).

        It receives the name of the file (file_name), the path of the file and the directory where
        this file is present (actually I realyze just now that I only needed the path of the file)

        It only deals with two files: name.basic and title.basic, both downloaded by the IMDB website.
        If everything works correctly, you should need to use this method only once
        """
        # from the properties take the name of the schema (we can be using more than one schema)
        schema = self.properties_data["schema"]

        counter = 0
        # now one if for every kind of file that we are considering
        if file_name == "name.basics.tsv":
            with open(file_path) as tsvfile:
                reader = csv.reader(tsvfile, delimiter='\t')
                next(reader)  # avoid the first line with the attribute names
                for row in reader:
                    counter = counter + 1
                    nconst = row[0]  # id of the record
                    primary_name = row[1].replace("'", "''")  # name of the person
                    birth_year = row[2].replace("\\N", "NULL")  # birth year of the person
                    death_year = row[3].replace("\\N", "NULL")  # death year of the person
                    professions = row[4].split(",")  # the profession need to be splitted
                    known_for_titles = row[5].split(",")  # the titles need to be splitted
                    # prepare the insertion
                    SQL_INSERT = "INSERT INTO %s.name (" \
                                 "nconst, primaryname, birthyear, deathyear)" \
                                 " VALUES ('%s', '%s', %s, %s)" % (schema, nconst, primary_name, birth_year, death_year)
                    self.cur.execute(SQL_INSERT)  # perform the query
                    # now we upload the has_profession table
                    for profession in professions:
                        SQL_INSERT_PROFESSION = "INSERT INTO %s.has_profession( " \
                                                "person_id, profession) VALUES ('%s', '%s');" % (
                                                    schema, nconst, profession)
                        self.cur.execute(SQL_INSERT_PROFESSION)
                        counter = counter + 1

                    # now we upload the titles for which the person is famous
                    for title in known_for_titles:
                        SQL_INSERT_TITLE = "INSERT INTO %s.known_for_title(person_id, title_id) VALUES ('%s', '%s');" % (
                            schema, nconst, title)
                        self.cur.execute(SQL_INSERT_TITLE)
                        counter = counter + 1

                    if (counter % 1000 == 0):
                        self.connection.commit()
                        print("committed " + str(counter) + " tuples from " + file_name)
                self.connection.commit()
                print("ended name.basics.tsv file")
        elif file_name == "title.basics.tsv":
            counter = 0
            with open(file_path) as tsvfile:
                reader = csv.reader(tsvfile, delimiter='\t')
                next(reader)  # eat up the line of the attributes
                for row in reader:
                    try:
                        tconst = row[0]
                        title_type = row[1].replace("\\N", "NULL")
                        primary_title = row[2].replace("\\N", "NULL").replace("'", "''")
                        start_year = row[5].replace("\\N", "NULL")
                        runtime_minutes = row[7].replace("\\N", "NULL")
                        genres = row[8].replace("\\N", "NULL").split(",")
                    except IndexError:
                        # it seemed that " in a csv file creates some problem for csv.reader. Should investigate further
                        print("error with row: ")
                        print(row)

                    SQL_INSERT_TITLE_ = "INSERT INTO %s.title( " \
                                        "tconst, " \
                                        "primarytitle," \
                                        " year, " \
                                        "runtimeminutes," \
                                        " author, " \
                                        "title_type) " \
                                        "VALUES ('%s', '%s', %s, %s, '%s', '%s');" % (
                                            schema, tconst, primary_title, start_year, runtime_minutes, 'NULL',
                                            title_type)
                    self.cur.execute(SQL_INSERT_TITLE_)
                    counter = counter + 1

                    for genre in genres:
                        SQL_INSERT_TITLE_GENRE = "INSERT INTO %s.has_genre( " \
                                                 "title_id, " \
                                                 "genre) " \
                                                 "VALUES ('%s', '%s');" % (schema, tconst, genre)
                        self.cur.execute(SQL_INSERT_TITLE_GENRE)
                    if (counter % 1000 == 0):
                        self.connection.commit()
                        print("committed " + str(counter) + " tuples from " + file_name)
                self.connection.commit()
                print("ended name.basics.tsv file")
        print("ended the import of tuples")

    def cleanAll(self):
        """ truncates all tables in a database. """
        schema = self.properties_data["schema"]
        SQL_ALL_TABLES = "SELECT table_name FROM information_schema.tables WHERE table_schema = '%s'" % (schema)
        self.cur.execute(SQL_ALL_TABLES)
        all = self.cur.fetchall()
        for table in all:
            SQL_TRUNCATE = "TRUNCATE TABLE " + schema + "." + table[0]
            self.cur.execute(SQL_TRUNCATE)
        self.connection.commit()
        print("truncate all the following tables: ")
        print(all)


def main1():
    """ test main #1. This main was meant to create tables and populate them (IMDB tables)
    I never finished one method, so it is useless
    @:rtype: object an object of some sort
    """
    execution = DatabaseToolkit()
    # create the tables
    execution.createTables()
    # insert the data into the database
    execution.insert_data_in_database()

    execution.shutDown()


def main2():
    """ test main # 2. Given that the tables are already in the database, populate them
    (we are talking about the IMDB database)
    """
    execution = DatabaseToolkit()
    execution.insert_imdb_files()
    execution.shutDown()


def main3():
    """ test main. Use this to truncate all tables in a schema.
    This method was used while I was testing some methods to import imdb
    and I needed to clean after my mess
    """
    execution = DatabaseToolkit()
    execution.cleanAll()
    execution.shutDown()


if __name__ == 'main':
    # main1() # useless
    main2()  # to upload the tables in imdb
    # main3() # to truncate everything
