# REALLY USEFUL METHODS

def keep_only_strings_with_this_substring(substring, string_list):
    """

    :param substring: we check if this string is present in the strings of the list
    :param string_list:
    :return: a list which is a sub-set of the given list, made only of strings with the substring
    """
    new_list = []
    for string in string_list:
        if substring in string:
            new_list.append(string)

    return new_list


def get_all_files_in_directory(directory_path):
    """
    Returns all the files in one directory (non-recursive) - it deals with the heinous .DS_Store
    :param directory_path:
    :return:
    """
    from os import walk
    # auxiliary list
    file_list = []
    # real list
    ultimate_list = []
    for (dirpath, dirname, filenames) in walk(directory_path):
        file_list.extend(filenames)

    # also, delete .DS_Store
    for string in file_list:
        if '.DS_Store' in string:
            file_list.remove(string)
        else:
            complete_path = dirpath + "/" + string
            ultimate_list.append(complete_path)

    return ultimate_list



def extract_column_from_csv_file(file_path, column_index=0):
    """
    Extract all the columns from a file, transpose them and returns them
    (the columns in the file becomes rows in the returned matrix)
    NB: the file should not contain a header
    :param file_path:
    :param column_index the index of the column that we want to isolate from the file
    :return: a matrix composed by rows that are the columns extracted from the file
    """
    import pandas as pd
    import numpy as np
    #get the dataframe
    file = pd.read_csv(file_path, header=None, usecols = [column_index, column_index])
    #isolate the column
    #file = file.iloc[:, column_index:(column_index+1)]
    # convert it into an array
    m = file.values
    # rotate the array, transpose it
    m = np.atleast_2d(m).T
    return m


def test_main_1():
    """ Test main """
    list = get_all_files_in_directory('/Users/dennisdosso/MEGAsync/Ricerca/credit/iuphar/queries')
    print(list)


def max_n_elements_from_list(list1, N):
    """
    Given a list, returns the first N biggest elements from that list
    :param list1:
    :param N:
    :return:
    """
    final_list = []

    for i in range(0, N):
        max1 = 0

        for j in range(len(list1)):
            if list1[j] > max1:
                max1 = list1[j];

        list1.remove(max1);
        final_list.append(max1)

    return final_list




if __name__ == '__main__':
    list = extract_column_from_csv_file('/Users/dennisdosso/MEGAsync/Ricerca/credit/iuphar/ids/families.csv', 0)
    print(list)