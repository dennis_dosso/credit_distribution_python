from typing import List, Any
from enum import Enum
from deprecated import deprecated

import psycopg2 as psy

# query to select all gpcr families
from queries.IUPHARqueries import *
from queries.iuphar_provenance_queries import *
from properties.properties import *


class IupharTypes(Enum):
    """This class represents the types of IUPHAR
    The values are the same used in the attribute type of the table family
    """
    GPCR = "gpcr"
    LGIC = "lgic"
    VGIC = "vgic"
    OTHER_IC = "other_ic"
    ENZIME = "enzyme"
    NHR = "nhr"
    CATALYTIC_REC = "catalytic_receptor"
    TRANSPORTER = "transporter"
    OTHERS = "other_protein"

class IUPHARDatabaseToolkit:
    """this is a class which contains methods to deal with IUPHAR """

    def __init__(self) -> object:
        # open the connection to the database
        self.connection = psy.connect(
            "host=" + host +
            " dbname=" + database_name +
            " user=" + user_name +
            " password=" + password)
        # prepare the cursor, which is used to perform queries to the database
        self.cur = self.connection.cursor()
        self._query_counter = 0

    def shutDown(self):
        """shuts down things such as the connection to the database and others.
        """
        self.cur.close()
        self.connection.close()

    @deprecated(reason="this method was a test. It does not work in general, but only for the fpcr families. "
                       "You should use get_all_iuphar_families_of_type")
    def get_all_gpcr_families(self) -> List[str]:
        """ Returns a list with all the ids of the dpcr families.
         This is a starting method, which returns all the ids that you are going to need later"""
        # list with the ids of the families we are interested in
        id_list: List[str] = []
        # execute the query
        self.cur.execute(select_al_gpcr_families)
        for table in self.cur.fetchall():
            # for every tuple in the result
            # take the id of the family, necessary for the query
            family_id = table[0]
            id_list.append(family_id)
        return id_list

    def get_all_iuphar_families_of_type(self, type__: Enum) -> object:
        """ Given a type of family (field 'type' in the table 'family'
        in IUPHAR) returns the id of all the families with that ID

        :param type__ an enum taken from the class Iuphar_types. """
        # list with the ids of the families we are interested in
        id_list: List[str] = []
        # execute the query
        self.cur.execute(select_iuphar_family % type__.value)
        for table in self.cur.fetchall():
            # for every tuple in the result
            # take the id of the family, necessary for the query
            family_id = table[0]
            id_list.append(family_id)
        return id_list

    def write_overview_queries(self, id_list, file):
        """ Writes the queries that are necessary for the first section of a family,
        the overview
        ----
        :param file: an open file in append where we will write our queries
        :type id_list: List of ids of IUPHAR families on which we iterate
        """
        # list of the queries that we produced
        for id_ in id_list:
            query = get_family_overview % (self._query_counter, id_)
            file.write(query)
            file.write("\n")
            self._query_counter = self._query_counter + 1

    def write_receptors_queries(self, id_list: List, f):
        """ Writes the queries that are necessary for the second section of a family,
        the list of receptors
        """
        # list of the queries that we produced
        for id in id_list:
            query = get_family_receptors % (self._query_counter, id)
            f.write(query)
            f.write("\n")
            self._query_counter = self._query_counter + 1

    def write_comments_queries(self, id_list: List, f):
        """ Writes the queries that are necessary for the third section of a family:
        the comments to a family
        """
        # list of the queries that we produced
        for id in id_list:
            query = get_family_comments % (self._query_counter, id)
            f.write(query)
            f.write("\n")
            self._query_counter = self._query_counter + 1

    def write_further_readings_queries(self, id_list: List, f):
        """ Writes the queries that are necessary for the fourth section of a family:
        the further readings of a family
        """
        for id in id_list:
            query = get_family_further_readings % (self._query_counter, id)
            f.write(query)
            f.write("\n")
            self._query_counter = self._query_counter + 1

    def write_contributors_queries(self, id_list: List, f):
        """ Writes the queries that are necessary for the sixth section of a family:
        the contributors to a family
        """
        # list of the queries that we produced
        for id in id_list:
            query = get_family_contributors % (self._query_counter, id)
            f.write(query)
            f.write("\n")
            self._query_counter = self._query_counter + 1

    # ----------------------- #

    def write_all_queries_for_class_gpcr(self, output_file_path):
        """Writes all the queries for the family class gpcr, corresponding to the webpages
        of the page
        https://www.guidetopharmacology.org/GRAC/ReceptorFamiliesForward?type=GPCR

        :param output_file_path name of the file where to write the queries
        """
        # open your file
        f = open(output_file_path, 'a')

        id_list = self.get_all_iuphar_families_of_type(IupharTypes.GPCR)
        self.write_overview_queries(id_list, f)
        self.write_receptors_queries(id_list, f)
        self.write_comments_queries(id_list, f)
        self.write_further_readings_queries(id_list, f)
        self.write_contributors_queries(id_list, f)

        f.close()

    def write_all_queries_for_class_lgic(self, f):
        """ This method (and the others like this one, that all share similar names)
        write on a file the queries in the provenance format that is used
        for the computation of the provenances of the output tuples.
        ----
        :param f: writer to the file where we want to write our queries
        """
        # get the ids you are interested in
        id_list = self.get_all_iuphar_families_of_type(IupharTypes.LGIC)
        self.write_overview_queries(id_list, f)
        # NB: in this case, this section is called sections and subunits, but it is the same: they are receptors
        self.write_receptors_queries(id_list, f)
        self.write_comments_queries(id_list, f)
        self.write_further_readings_queries(id_list, f)
        self.write_contributors_queries(id_list, f)

    def write_all_queries_for_class_ion_channels(self, output_file_path):
        """
        :param output_file_path: path of the file where to write the queries
        :return:
        """
        # open the file where we write the queries
        f = open(output_file_path, 'w')

        # get all the ids
        id_list = self.get_all_iuphar_families_of_type(IupharTypes.LGIC)
        id_list.extend(self.get_all_iuphar_families_of_type(IupharTypes.VGIC))
        id_list.extend(self.get_all_iuphar_families_of_type(IupharTypes.OTHER_IC))

        # write the queries for every section
        self.write_queries_that_form_up_a_webpage(id_list, f)

        f.close()

    def write_queries_for_enzimes(self, output_file_path):
        """
        :param output_file_path:
        :return:
        """
        f = open(output_file_path, 'w')
        id_list = self.get_all_iuphar_families_of_type(IupharTypes.ENZIME)

        self.write_queries_that_form_up_a_webpage(id_list, f)

        f.close()

    def write_queries_for_nhr(self, output_file_path):
        """
        :param output_file_path:
        :return:
        """
        f = open(output_file_path, 'w')
        id_list = self.get_all_iuphar_families_of_type(IupharTypes.NHR)

        self.write_queries_that_form_up_a_webpage(id_list, f)

        f.close()

    def write_queries_for_catalytic_receptors(self, output_file_path):
        """
        :param output_file_path:
        :return:
        """
        f = open(output_file_path, 'w')
        id_list = self.get_all_iuphar_families_of_type(IupharTypes.CATALYTIC_REC)

        self.write_queries_that_form_up_a_webpage(id_list, f)

        f.close()

    def write_queries_for_transporters(self, output_file_path):
        """
        :param output_file_path:
        :return:
        """
        f = open(output_file_path, 'w')
        # get the ids
        id_list = self.get_all_iuphar_families_of_type(IupharTypes.TRANSPORTER)
        # write the queries
        self.write_queries_that_form_up_a_webpage(id_list, f)

        f.close()

    def write_queries_for_other_proteins(self, output_file_path):
        """
        :param output_file_path:
        :return:
        """
        f = open(output_file_path, 'w')
        # get the ids
        id_list = self.get_all_iuphar_families_of_type(IupharTypes.OTHERS)
        # write the queries
        self.write_queries_that_form_up_a_webpage(id_list, f)

        f.close()

    def write_queries_from_families_whose_ids_are_taken_from_csv_file(self, csv_file_path: str, output_file_path: str):
        """
        Given a file containing ids of IUPHAR FAMILIES (not ligands and receptors, but specifically families)
        it reads these ids and builds the necessary queries.
        :param output_file_path: path of the file where we are writing our queries
        :param csv_file_path: path of the csv file containing in one column the ids that represent your queries
        :return:
        """
        from modules import rum
        # first, let us take these ids from the file, we also read from the first column
        id_list = rum.extract_column_from_csv_file(csv_file_path, 0)[0]
        # open the output file where we are going to write
        f = open(output_file_path, 'w')
        self.write_queries_that_form_up_a_webpage(id_list, f)

    def write_queries_that_form_up_a_webpage(self, id_list, f):
        """
        Writes on the file writer a series of queries corresponding to the basic structure of a webpage
        of iuphar, i.e. the queries that you need to gather the information for one webpage (circa, not everything).

        :param id_list: list with the ids of the element for which we need to build queries
        :param f: the file writer where we are writing
        :return:
        """
        self.write_overview_queries(id_list, f)
        self.write_receptors_queries(id_list, f)
        self.write_comments_queries(id_list, f)
        self.write_further_readings_queries(id_list, f)
        self.write_contributors_queries(id_list, f)

    def compute_total_credit_generated_from_family_query_with_id(self, id: str):
        """
        Given the id of a family, it performs a series of count sql queries to count the total number
        of tuples produced by the query. These total number of tuples count as 1 in the generation of credit.
        For now we are using this basic assignment, credit=1 for each tuple. In the future we
        may want to be more sophisticated.
        :param id:
        :return: an integer which is the result of the sum of all these counts
        """
        total_count = 0

        sql_query = sql_count_get_family_overview % id
        total_count = total_count + self.perform_this_count_query(sql_query)

        sql_query = sql_count_get_family_receptors % id
        total_count = total_count + self.perform_this_count_query(sql_query)

        sql_query = sql_count_get_family_comments % id
        total_count = total_count + self.perform_this_count_query(sql_query)

        sql_query = sql_count_get_family_further_reading % id
        total_count = total_count + self.perform_this_count_query(sql_query)

        sql_query = sql_count_get_family_contributors % id
        total_count = total_count + self.perform_this_count_query(sql_query)

        return total_count

    def perform_this_count_query(self, count_query: str) -> int:
        """
        Sort of private method, adds the result of a count query to a counter
        :param query:
        :param counter:
        :return: the result of the count query
        """
        self.cur.execute(count_query)
        for table in self.cur.fetchall():
            # surely only one result, anyway, take the count
            count = table[0]
        return count

    def get_all_the_autors_associated_to_the_family_with_this_id(self, id_: int):
        """
        Given the id of a family, this class performs one query
        :param id_: id of the family of which we want to know the authors
        :return: two lists, the first one with the names of the authors, the second one with their anonymous ids
        """
        # list which is going to contain all the authors of this query
        author_list = []
        id_list = []
        sql_query = sql_get_names_and_surnames_of_authors_of_a_family_specifying_the_family_id % id_
        self.cur.execute(sql_query)
        for result in self.cur.fetchall():
            #for each author that we have
            first_names = result[0]
            second_names = result[1]
            anonymous_id = result[2]
            complete_name = first_names + " " + second_names
            author_list.append(complete_name)
            id_list.append(anonymous_id)
        return author_list, id_list

    def get_name_of_the_family_with_this_id(self, id_: int):
        #nprepare query
        sql_query = sql_get_name_of_family_with_this_id % id_
        self.cur.execute(sql_query)
        name = ''
        for tuple in self.cur.fetchall():
            name += tuple[0]
        return name

if __name__ == '__main__':
    execution = IUPHARDatabaseToolkit()
    total_credit = execution.compute_total_credit_generated_from_family_query_with_id('180')
    execution.shutDown()
