#this files contain queries in the format used to extract provenance


# ---------- these are the queries to build a page of a family ------------ #
# section 1: get the overview of a family
get_family_overview: str = "Q%s|gft.overview|gft|gft.family_id = '%s'||gft:grac_family_text"

# section 2: receptors of a family
get_family_receptors = "Q%s| o.name |f,r2f,rb,o|f.family_id = r2f.family_id,r2f.object_id = rb.object_id," \
                       "rb.object_id = o.object_id,f.family_id = '%s' ||f:family, r2f:receptor2family, " \
                       "rb:receptor_basic, o:object"

# section 3: comments of a family
get_family_comments = "Q%s|gft.comments|gft|gft.family_id = '%s'||gft:grac_family_text"

# section 4: further readings
get_family_further_readings = "Q%s | r.article_title, r.authors, r.year , r.pages, r.publication, " \
                              "r.publisher, r.authors_vector " \
                              "|f, gfr, r" \
                              "|f.family_id = gfr.family_id, gfr.reference_id = r.reference_id, f.family_id = '%s'" \
                              "|" \
                              "|f:family, gfr:grac_further_reading, r:reference"

# section 5: get the references of a family
get_family_references = "I have absolutely no idea as of now"


# section 6: get the contributors (authors) of a family
get_family_contributors = "Q%s" \
                          "| c.first_names , c.surname" \
                          "|cf, c" \
                          "|cf.contributor_id  = c.contributor_id, cf.family_id = '%s'" \
                          "|" \
                          "|cf:contributor2family, c:contributor"

# ----- query to get the ids of the families with UK author ----- #
get_family_uk_author = "Q%s" \
                       "|f.family_id" \
                       "|f, cf, c" \
                       "|f.family_id = cf.family_id, cf.contributor_id = c.contributor_id, f.family_id = '%s'" \
                       "|" \
                       "|f:family,cf:contributor2family,c:contributor"
