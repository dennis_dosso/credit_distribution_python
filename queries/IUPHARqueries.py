#query to select all the families in iuphar of type gpcr
select_al_gpcr_families = 'select * from family where type = \'gpcr\' order by family_id'

select_iuphar_family = "SELECT * from family where type = '%s' order by family_id"

# ----- SQL queries to create web-pages ----- #

sql_count_get_family_overview='select count(gft.overview) from grac_family_text gft where gft.family_id = %s'

sql_count_get_family_receptors='select count(*) from family as f join receptor2family as r2f ' \
                               'on f.family_id = r2f.family_id join receptor_basic as rb on ' \
                               'r2f.object_id = rb.object_id join object as o on ' \
                               'rb.object_id = o.object_id where ' \
                               'f.family_id=%s'

sql_count_get_family_comments='select count(*) ' \
                              'from grac_family_text gft ' \
                              'where gft.family_id = %s'

sql_count_get_family_further_reading='select count(*)' \
                                     ' from "family" f join grac_further_reading as gfr on' \
                                     ' f.family_id = gfr.family_id join reference as r ' \
                                     'on gfr.reference_id = r.reference_id' \
                                     ' where f.family_id = %s'

sql_count_get_family_references = "I have absolutely no idea as of now"

sql_count_get_family_contributors='select count(*) ' \
                                  'from contributor2family cf join contributor c ' \
                                  'on cf.contributor_id  = c.contributor_id ' \
                                  'where cf.family_id = %s'


# ----- queries to deal with authors ----- #
sql_get_names_and_surnames_of_authors_of_a_family_specifying_the_family_id=\
    'select c.first_names , c.surname, c.anonymous_id ' \
    'from contributor2family cf  join contributor c ' \
    'on cf.contributor_id  = c.contributor_id ' \
    'where cf.family_id = %s'

# ----- to get the name of a family with a certain id ----- #
sql_get_name_of_family_with_this_id='select f.name from family f where f.family_id = %s'

# ----- to get the families produced by a UK author ----- #
sql_get_uk_families = "select distinct f.family_id from family" \
                      " f join contributor2family cf on f.family_id = cf.family_id " \
                      "join contributor c on cf.contributor_id = c.contributor_id " \
                      "where c.country = 'UK'"

sql_get_tuple_id = 'select f."c||prov" from family f where f.family_id = %s'
