class AuthorsMap:
    """A class containing authors, their number of citations and their quantity of credit"""

    def __init__(self) -> object:
        # define an empty dictionary
        self.authors_map = {}

    def add_an_author(self, author_id: str):
        """
        Adds an author to this dictionary. It puts the credit and the number of citations to 0
        The author is added only if it was not already present, otherwise everything is
        left untouched
        :param author_id: name of the author
        :return:
        """
        if author_id not in self.authors_map:
            self.authors_map[author_id] = {'credit': 0, 'citations': 0, 'name': ''}

    def add_name_to_author(self, author_id, author_name):
        """
        Given the id of an author, it adds a name string
        :param author_id:
        :param author_name:
        :return:
        """
        self.authors_map[author_id]['name'] = author_name

    def add_list_of_authors(self, authors_ids):
        """
        Adds a list of authors to this
        :param authors_ids: a list of strings with the ids
        :return:
        """
        for id_ in authors_ids:
            self.add_an_author(id_)

    def add_citation_to_author(self, author_id):
        """
        Adds +1 to the citation count of the selected author
        :param author_id: name of the author you want to add a +1 to the citation count
        :return:
        """
        self._add_quantity_to_a_certain_author_to_a_certain_field(1, author_id, 'citations')

    def add_credit_to_author(self, credit: float, author_id):
        """
        adds the specified quantity to the credit possessed by a certain author
        :param author_id:
        :param credit:
        :return:
        """
        self._add_quantity_to_a_certain_author_to_a_certain_field(credit, author_id, 'credit')

    def _add_quantity_to_a_certain_author_to_a_certain_field(self, quantity: float, author_id: str, field_name: str):
        data_ = self.authors_map[author_id][field_name]
        data_ = data_ + quantity
        self.authors_map[author_id][field_name] = data_

    def print_content_in_a_csv_file(self, output_file: str) -> None:
        """
        Prints the content in a csv file in three columns: name of the author, number of citations, credit
        :param output_file:
        :return:
        """
        writer = open(output_file, 'w')
        writer.write('author_id,author_name,citation_count,credit,average_credit\n')
        for author in self.authors_map:
            name = self.authors_map[author]['name'].replace(',', ' ')
            citations = self.authors_map[author]['citations']
            credit = self.authors_map[author]['credit']
            average_credit = float(credit) / float(citations)
            writer.write(str(author) + ',' + name + ',' + str(citations) + ',' + str(credit) + ',' + str(average_credit)
                         + '\n')
        writer.close()

