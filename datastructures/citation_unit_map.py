class CUMap:
    """A class containing citation units, their number of citations and their quantity of credit"""

    def __init__(self) -> object:
        # define an empty dictionary
        self.entities_map = {}

    def add_a_citation_unit(self, entity_id: str):
        """
        Adds an author to this dictionary. It puts the credit and the number of citations to 0
        The author is added only if it was not already present, otherwise everything is
        left untouched
        :param entity_id: name of the author
        :return:
        """
        if entity_id not in self.entities_map:
            self.entities_map[entity_id] = {'credit': 0, 'citations': 0, 'name': ''}

    def add_name_to_citation_unit(self, unit_id, unit_name):
        """
        Given the id of an author, it adds a name string
        :param unit_id: id of the entity (e.g. its DOI or a generic internal ID)
        :param unit_name: name of the unit (it may be the name of the paper, of the book etc.)
        :return:
        """
        self.entities_map[unit_id]['name'] = unit_name

    def add_list_of_citation_units(self, unit_ids):
        """
        Adds a list of authors to this
        :param authors_ids: a list of strings with the ids
        :return:
        """
        for id_ in unit_ids:
            self.add_a_citation_unit(id_)

    def add_citation_to_the_unit(self, unit_id):
        """
        Adds +1 to the citation count of the selected author
        :param unit_id: name of the author you want to add a +1 to the citation count
        :return:
        """
        self._add_quantity_to_a_certain_unit_to_a_certain_field(1, unit_id, 'citations')

    def add_credit_to_unit(self, credit: float, unit_id):
        """
        adds the specified quantity to the credit possessed by a certain author
        :param unit_id:
        :param credit:
        :return:
        """
        self._add_quantity_to_a_certain_unit_to_a_certain_field(credit, unit_id, 'credit')

    def _add_quantity_to_a_certain_unit_to_a_certain_field(self, quantity: float, author_id: str, field_name: str):
        data_ = self.entities_map[author_id][field_name]
        data_ = data_ + quantity
        self.entities_map[author_id][field_name] = data_

    def print_content_in_a_csv_file(self, output_file: str) -> None:
        """
        Prints the content in a csv file in three columns: name of the author, number of citations, credit
        :param output_file:
        :return:
        """
        writer = open(output_file, 'w')
        writer.write('id,name,citation_count,credit,average_credit\n')
        for entity in self.entities_map:
            name = self.entities_map[entity]['name'].replace(',', ' ')
            citations = self.entities_map[entity]['citations']
            credit = self.entities_map[entity]['credit']
            if float(citations) != 0:
                average_credit = float(credit) / float(citations)
            else:
                average_credit = 0
            writer.write(str(entity) + ',' + name + ',' + str(citations) + ',' + str(credit) + ',' + str(average_credit)
                         + '\n')
        writer.close()

