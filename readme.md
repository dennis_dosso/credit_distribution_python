###Documentation of the pycredit package

Hello there, this is the pycredit package, the one I used, together 
with a Java little library, to deal with my the credit distribution 
project. Some things that are needed to understand what is happening here:

__DatabaseToolkit.py__ 
In this class you have some useful methods to deal with databases. 
In particular:

_createTables_: use this method to execute the create table SQL commands
in the property file (which is a json)

_deal_with_one_imdb_file_:
it reads all the tsv files inside one directory and uploads them in the database.
This method is hardcoded and can only deal with the name.basics and
title.basics.tsv files from the [IMDB site](https://datasets.imdbws.com/)

_shutDown_
since this class when created creates a connection to database, 
you should close it when you are done to keep things clean.

_cleanAll_
performs a truncate table on every table in the database.

_main1_
This was a test main that I used to insert all the files

_main2_
used to clean the database. 