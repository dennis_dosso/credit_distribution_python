# this is only a 'property' file containing values that you may need in other scripts


# credentials to access a database
host = "localhost"
database_name = "iuphar"  # name of the database CHECK THAT THIS IS CORRECT
# database_name = "credit" # name of the database CHECK THAT THIS IS CORRECT
user_name = "postgres"
password = "Ulisse92"
schema = "public"  # schema that we are using inside the database CHECK THAT THIS IS CORRECT

columns_in_hitmap = 15

# file where we write out queries
output_query_file = "/Users/dennisdosso/MEGAsync/Ricerca/credit/iuphar/queries/iuphar_queries.txt"

output_gpcr_queries = "/Users/dennisdosso/MEGAsync/Ricerca/credit/queries/iuphar_gpcr_queries.txt"

output_ion_queries = "/Users/dennisdosso/MEGAsync/Ricerca/credit/queries/iuphar_ion_queries.txt"

output_enzime_queries = "/Users/dennisdosso/MEGAsync/Ricerca/credit/queries/iuphar_enzime_queries.txt"

output_nhr_queries = "/Users/dennisdosso/MEGAsync/Ricerca/credit/queries/iuphar_nhr_queries.txt"

output_catalytic_receptors_queries = "/Users/dennisdosso/MEGAsync/Ricerca/credit/queries/iuphar_catalytic_receptors_queries.txt"

output_transporters_queries = "/Users/dennisdosso/MEGAsync/Ricerca/credit/queries/iuphar_transporters_queries.txt"

output_other_proteins_queries = "/Users/dennisdosso/MEGAsync/Ricerca/credit/queries/iuphar_other_proteins_queries.txt"

#path of the csv file containing the ids of the families used in queries extracted from the iuphar papers
list_of_ids_of_familes_used_in_iuphar_papers='/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/ids/target_families_citations.txt'

#path of the output files where to write queries obtained from the papers
output_queries_from_papers = '/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/queries/iuphar_queries_from_papers.txt'

#directory where all iuphar papers are saved
pdf_directory='/Users/dennisdosso/MEGAsync/Ricerca/progetti di ricerca/Credit Distribution/iuphar/papers'

#paths of the files where we write the ids of the elements used in the iuphar papers
family_output_file='/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/ids/target_families_citations.txt'
ligand_output_file = '/Users/dennisdosso/MEGAsync/Ricerca/progetti di ricerca/Credit Distribution/iuphar/ids/ligands.txt'
object_output_file='/Users/dennisdosso/MEGAsync/Ricerca/progetti di ricerca/Credit Distribution/iuphar/ids/objects.txt'


# ----- print_iuphar_single_columns_tables ----- #
#change these two together
# suffix to put on the files to distinguish them
suffix = "_synthetic"
# suffix = "_how_oriented_synthetic_solution_2"
#directory where to save the heatmaps
output_hitmaps_directory = "/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/heatmap/progressive"
#directory where we save the csv files with the credit information for every table (so you do not have)
#to re-distribute the credit)
output_csv_directory = "/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/heatmap/progressive"

# ----- distribution_credit_to_authors -----#
authors_map_output_file = '/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/csv/citations_vs_credit/papers/authors_with_citations_and_credit_extracted_from_papers.csv'
families_map_output_file = '/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/csv/citations_vs_credit/papers/families_with_citations_and_credit_extracted_from_papers.csv'

synthetic_queries_csv_file = '/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/csv/citations_vs_credit/synthetic/authors_citations_credit_100_2.csv'

# -- output file where to write the queries for the how-oriented test -- #
output_how_oriented_queries='/Users/dennisdosso/MEGAsync/Ricerca/PROGETTI/Credit Distribution/iuphar/queries/how_oriented_queries.txt'
ids_file='/Users/dennisdosso/MEGAsync/Ricerca/PROGETTI/Credit Distribution/iuphar/queries/IDs.txt'
tuple_ids_file='/Users/dennisdosso/MEGAsync/Ricerca/PROGETTI/Credit Distribution/iuphar/queries/tuple_ids.txt'

# ---------- input file for the correlation coefficients ------------ #
csv_input_file="/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/csv/citations_vs_credit/synthetic/ordered/10K.txt"
