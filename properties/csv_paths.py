csv_directory = "/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/heatmap/progressive/10K"

output_directory = "/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/heatmap/progressive/10K/little_heatmaps"
# output_directory = "/Users/dennisdosso/MEGAsync/Ricerca/progetti_di_ricerca/Credit Distribution/iuphar/heatmap/progressive/10K/big_heatmaps"

lin = "family_lineage_credit_synthetic.csv"
why = "family_ why_credit_synthetic.csv"
how = "family_how_credit_synthetic.csv"

# 78-88/ 264 - 274/ 4 - 14
starting_index = 391
end_index = 401

# for the whole family table
# starting_index = 0
# end_index = 794

# max value in the heatmaps
vamx=9.47
