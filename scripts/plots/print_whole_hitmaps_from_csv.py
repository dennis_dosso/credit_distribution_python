import pandas as pd
import numpy as np

from properties.csv_paths import csv_directory as input_directory
from properties.csv_paths import output_directory

from properties.csv_paths import lin
from properties.csv_paths import why
from properties.csv_paths import how

from properties.csv_paths import starting_index as start
from properties.csv_paths import end_index as end
from properties.csv_paths import vamx

from math import floor

from modules.GenerateHeatmapsFromTables import HeatMapFromDBGenerator

from properties.properties import columns_in_hitmap

""" Script to produce the heatmaps from a csv file 
(so you do not have to always distribute credit """



# read the data from the csv file. Take only a part of it, and prepare the values array
lin_values = pd.read_csv(input_directory + "/" + lin, header=None)
m = lin_values[start:end].values
# transpose the array
m = np.atleast_2d(m).T
m = m[
    0]  # need this little trick because the transpose is an array of arrays with only one array, so I take that one array

t = floor(len(m) / columns_in_hitmap)


HeatMapFromDBGenerator.print_one_column_from_csv(tuples_in_one_column=t,
                                                 columns_=columns_in_hitmap,
                                                 values=m,
                                                 table_name="1_lineage", file_format="pdf",
                                                 output_hitmaps_directory_=output_directory,
                                                 vmax_=vamx)

# do the same for why prov
lin_values = pd.read_csv(input_directory + "/" + why, header=None)
m = lin_values[start:end].values
m = np.atleast_2d(m).T
m = m[0]

HeatMapFromDBGenerator.print_one_column_from_csv(tuples_in_one_column=t,
                                                 columns_=columns_in_hitmap,
                                                 values=m,
                                                 table_name="2_why", file_format="pdf",
                                                 output_hitmaps_directory_=output_directory,
                                                 vmax_=vamx)

# do the same for how prov
lin_values = pd.read_csv(input_directory + "/" + how, header=None)
m = lin_values[start:end].values
m = np.atleast_2d(m).T
m = m[0]

HeatMapFromDBGenerator.print_one_column_from_csv(tuples_in_one_column=t,
                                                 columns_=columns_in_hitmap,
                                                 values=m,
                                                 table_name="3_how", file_format="pdf",
                                                 output_hitmaps_directory_=output_directory,
                                                 vmax_=vamx)
