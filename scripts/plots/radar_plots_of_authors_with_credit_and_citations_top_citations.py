import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from math import pi
from properties.properties import *
from properties.plot_paths import *
from modules.rum import max_n_elements_from_list

# import the data
# read the csv file
table = pd.read_csv(authors_map_output_file)
# take the top 10 elements of the credit. Order by citation
radar_credit_via_credit = table.sort_values(by=['citation_count'], ascending=False).head(20)['credit']
# find the biggest element among these
max_ = table['credit'].max()
# normalize
radar_credit_via_credit = radar_credit_via_credit / max_
#now convert to list
radar_credit_via_credit = radar_credit_via_credit.values.flatten().tolist()
#since I need to close the circle, I add a copy of the first elements at the end of the list
radar_credit_via_credit += radar_credit_via_credit[:1]


#do the same for the citations
radar_citation_via_credit = table.sort_values(by=['citation_count'], ascending=False).head(20)[
    'citation_count']
#find the max value for the citations
max_ = table['citation_count'].max()
#normalize the citations
radar_citation_via_credit = radar_citation_via_credit / max_
#convert to list
radar_citation_via_credit = radar_citation_via_credit.values.flatten().tolist()
radar_citation_via_credit += radar_citation_via_credit[:1]



# ------- PART 1: Create background
N = len(radar_credit_via_credit) - 1

# What will be the angle of each axis in the plot? (we divide the plot / number of variable)
angles = [n / float(N) * 2 * pi for n in range(N)]
angles += angles[:1]

# Initialise the spider plot
ax = plt.subplot(111, polar=True)

# If you want the first axis to be on top:
ax.set_theta_offset(pi / 2)
ax.set_theta_direction(-1)

# labels on the points of the radar
labels = table.sort_values(by=['citation_count'], ascending=False).head(20)[
    'author_id']

# Draw one axe per variable + add labels labels yet
plt.xticks(angles[:-1], labels)

# # Draw ylabels
ax.set_rlabel_position(0)
plt.yticks([.25, .50, .75], [".25", ".50", ".75"], color="grey", size=7)
plt.ylim(0,1) # set the max value of the y axis

# ----- PART 2: Add plots

# points for the credit
ax.plot(angles, radar_citation_via_credit, linewidth=1, linestyle='solid', label="citations")
ax.fill(angles, radar_citation_via_credit, 'b', alpha=0.2)

ax.plot(angles, radar_credit_via_credit, linewidth=1, linestyle='solid', label="credit")
ax.fill(angles, radar_credit_via_credit, 'r', alpha=0.2)

# Add legend
plt.legend(loc='upper right', bbox_to_anchor=(0.1, 0.1))

plt.title('top 20 authors citation-wise')
plt.savefig(radar_top_citations)
plt.show()
