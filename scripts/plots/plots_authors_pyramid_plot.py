import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from properties.properties import *
from properties.plot_paths import *
import scipy.stats as stat

# read the csv file
table = pd.read_csv(authors_map_output_file)
# isolate the column with citations
citation_counts = table[['citation_count']].values
citation_counts = np.atleast_2d(citation_counts).T
citation_counts = citation_counts[0]
#find the highest value
citation_max = np.amax(citation_counts)
#normalize everything
citation_counts = citation_counts / citation_max

#do the same, this time with the credit
credits_ = table[['credit']]
credits_ = credits_.values
credits_ = np.atleast_2d(credits_).T
credits_ = credits_[0]
credit_max = np.amax(credits_)
#normalize values
credits_ = credits_ / credit_max

#now ready to create figures
#first, create more than one plot
fig, axes = plt.subplots(nrows=1, ncols=2, sharey=True) #set sharey to True to share the y axis
ax0, ax1 = axes.flatten()


#number of bins where to put our elements
bins = np.arange(1, len(citation_counts)+1)

ax0.barh(bins, citation_counts, align='center')
ax0.set(title='citations')
ax0.invert_xaxis()


#what you want is NOT an histogram. An histogram accumulates value in the bins. What you want is a BAR plot, son
ax1.barh(bins, credits_, align='center', color='red')
ax1.set(title='credit')
ax1.set_ylabel('authors')

fig.tight_layout()
fig.subplots_adjust(wspace=0.09)

plt.savefig(pyramid_path)
# plt.show()




