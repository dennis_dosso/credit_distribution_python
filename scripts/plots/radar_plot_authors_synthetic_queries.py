import pandas as pd

from math import pi
import matplotlib.pyplot as plt
from properties.properties import synthetic_queries_csv_file
from properties.plot_paths import radar_top_synthetic

""" Produces a radar plot with all 3 provenances. Takes a csv file as input.
Since I am silly, I usually do not put the header in that file.
In case of error, this is the header to use:
author_id,citation_count,lineage,why,how
"""
# read the csv file where I put the data
table = pd.read_csv(synthetic_queries_csv_file)

#BY ORDER OF CITATIONS

#CITATIONS
# Order by the number of citations. Take the first 20 elements
radar_citation_orderby_citation = table.sort_values(by=['how'], ascending=False).head(20)['citation_count']
# find the biggest element among these
max_ = table['citation_count'].max()
# normalize
radar_citation_orderby_citation = radar_citation_orderby_citation / max_
#now convert to list
radar_citation_orderby_citation = radar_citation_orderby_citation.values.flatten().tolist()
#since I need to close the circle, I add a copy of the first elements at the end of the list
radar_citation_orderby_citation += radar_citation_orderby_citation[:1]

#LINEAGE
radar_lineage_via_credit = table.sort_values(by=['how'], ascending=False).head(20)[
    'lineage']
max_ = table['lineage'].max()
radar_lineage_via_credit = radar_lineage_via_credit / max_
radar_lineage_via_credit = radar_lineage_via_credit.values.flatten().tolist()
radar_lineage_via_credit += radar_lineage_via_credit[:1]

#WHY-PROV
radar_why_via_credit = table.sort_values(by=['how'], ascending=False).head(20)[
    'why']
max_ = table['why'].max()
radar_why_via_credit = radar_why_via_credit / max_
radar_why_via_credit = radar_why_via_credit.values.flatten().tolist()
radar_why_via_credit += radar_why_via_credit[:1]

#HOW-PROV
radar_how_orderby_citation = table.sort_values(by=['how'], ascending=False).head(20)[
    'how']
max_ = table['how'].max()
radar_how_orderby_citation = radar_how_orderby_citation / max_
radar_how_orderby_citation = radar_how_orderby_citation.values.flatten().tolist()
radar_how_orderby_citation += radar_how_orderby_citation[:1]

#we need the labels
labels = table.sort_values(by=['how'], ascending=False).head(20)[
    'author_id']


# ------- PART 1: Create background
N = len(radar_citation_orderby_citation) - 1

# What will be the angle of each axis in the plot? (we divide the plot / number of variable)
angles = [n / float(N) * 2 * pi for n in range(N)]
angles += angles[:1]

# Initialise the spider plot
ax = plt.subplot(111, polar=True)

# If you want the first axis to be on top:
ax.set_theta_offset(pi / 2)
ax.set_theta_direction(-1)

# Draw one axe per variable + add labels labels yet
plt.xticks(angles[:-1], labels)

# # Draw ylabels
ax.set_rlabel_position(0)
plt.yticks([.25, .50, .75], [".25", ".50", ".75"], color="grey", size=7)
plt.ylim(0,1) # set the max value of the y axis

# ----- PART 2: Add plots

# points for the credit
ax.plot(angles, radar_citation_orderby_citation, linewidth=1, linestyle='solid', label="citations")
ax.fill(angles, radar_citation_orderby_citation, 'b', alpha=0.2)

ax.plot(angles, radar_lineage_via_credit, linewidth=1, linestyle='solid', label="lineage credit")
# ax.fill(angles, radar_lineage_via_credit, 'r', alpha=0.2)

ax.plot(angles, radar_why_via_credit, linewidth=1, linestyle='solid', label="why-prov credit")
# ax.fill(angles, radar_why_via_credit, 'y', alpha=0.2)

ax.plot(angles, radar_how_orderby_citation, linewidth=1, linestyle='solid', label="how-prov credit")
# ax.fill(angles, radar_how_orderby_citation, 'g', alpha=0.2)


# Add legend
plt.legend(loc='upper right', bbox_to_anchor=(0.1, 0.1))

plt.title('top 20 authors citation-wise')
plt.savefig(radar_top_synthetic)
plt.show()