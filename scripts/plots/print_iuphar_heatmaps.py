from modules.GenerateHeatmapsFromTables import HeatMapFromDBGenerator
from properties.properties import columns_in_hitmap
from properties.properties import suffix as s

# this is the path of the directory where you'll save the heat-maps
from properties.properties import output_hitmaps_directory

# output_csv_directory is the property where to save the csv files

# main script for creating heatmaps from the DB and write csv files with the information
# the heatmaps are saved in a directory specified in the properties, output_hitmaps_directory
# also, csv files are saved so you do not need to re-distribute the credit.


heatGenerator = HeatMapFromDBGenerator()

# heatGenerator.print_one_table_one_column_from_DB("family", "lineage_credit", columns_=columns_in_hitmap, suffix_=s, file_format='pdf')
# heatGenerator.print_one_table_one_column_from_DB("family", " why_credit", columns_=columns_in_hitmap, suffix_=s, file_format='pdf')
# heatGenerator.print_one_table_one_column_from_DB("receptor2family", columns_=columns_in_hitmap, suffix_=s)
# heatGenerator.print_one_table_one_column_from_DB("receptor_basic", columns_=columns_in_hitmap, suffix_=s)
# heatGenerator.print_one_table_one_column_from_DB("object", columns_=columns_in_hitmap, suffix_=s)
# heatGenerator.print_one_table_one_column_from_DB("grac_further_reading", columns_=columns_in_hitmap, suffix_=s)
# heatGenerator.print_one_table_one_column_from_DB("reference", suffix_=s)
# heatGenerator.print_one_table_one_column_from_DB("contributor2family", columns_=columns_in_hitmap, suffix_=s)
# heatGenerator.print_one_table_one_column_from_DB("contributor", columns_=columns_in_hitmap, suffix_=s)


# --- this the script to deal with three different provenances on the same table --- #
heatGenerator.print_one_table_one_column_from_DB("family", "lineage_credit", columns_=columns_in_hitmap,
                                                 suffix_=s + "_lineage", file_format='pdf')
heatGenerator.print_one_table_one_column_from_DB("family", " why_credit", columns_=columns_in_hitmap, suffix_=s + "_why", file_format='pdf')
heatGenerator.print_one_table_one_column_from_DB("family", "how_credit", columns_=columns_in_hitmap, suffix_=s + "_how", file_format='pdf')

# heatGenerator.print_one_table_one_column_from_DB("contributor", "lineage_credit", columns_=columns_in_hitmap,
#                                                  suffix_=s + "_lineage", file_format='pdf')
# heatGenerator.print_one_table_one_column_from_DB("contributor", " why_credit", columns_=columns_in_hitmap, suffix_=s + "_why", file_format='pdf')
# heatGenerator.print_one_table_one_column_from_DB("contributor", "how_credit", columns_=columns_in_hitmap, suffix_=s + "_how", file_format='pdf')
