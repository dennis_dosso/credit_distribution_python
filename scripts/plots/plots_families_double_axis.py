import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from properties.properties import *
from properties.plot_paths import *
import scipy.stats as stat

# read the csv file
table = pd.read_csv(families_map_output_file)
# isolate the column with citations
citation_counts = table[['citation_count']].values
#transpose
citation_counts = np.atleast_2d(citation_counts).T
# remove header
citation_counts = citation_counts[0]
#find the highest value
citation_max = np.amax(citation_counts)
#normalize everything
citation_counts = citation_counts / citation_max

#do the same, this time with the credit
credits_ = table[['credit']]
credits_ = credits_.values
credits_ = np.atleast_2d(credits_).T
credits_ = credits_[0]
credit_max = np.amax(credits_)
credits_ = credits_ / credit_max

#produce a stupid-ass graphic
bins = np.arange(1, len(citation_counts)+1)

fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel('queries')
ax1.set_ylabel('citations')
ax1.plot(bins, citation_counts, color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('credit', color=color)  # we already handled the x-label with ax1
ax2.plot(bins, credits_, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.savefig(double_axis_queries_path)
# plt.show()

#compute the Pearson coefficient
linear_correlation = stat.pearsonr(credits_, citation_counts)

#compute the Kendall's tau coefficient (I suppose it is useless, but that is)
rank_correlation = stat.kendalltau(credits_, citation_counts)

# Spearman's rank coefficient
spearman_correlation = stat.spearmanr(credits_, citation_counts)

print('linear correlation: ' + str(linear_correlation))
print('rank correlation: ' + str(rank_correlation))
print('spearman correlation: ' + str(spearman_correlation))


