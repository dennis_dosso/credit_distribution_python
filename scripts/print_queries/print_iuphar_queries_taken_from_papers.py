from modules import IUPHARDatabaseToolkit
from properties.properties import * # all the properties values

""" once you have produced a csv file containing the ids of the queries from the papers 
(script read_all_iuphar_urls_from_papers), use them to produce the queries in the 
provenance format 
"""
execution = IUPHARDatabaseToolkit.IUPHARDatabaseToolkit()

execution.write_queries_from_families_whose_ids_are_taken_from_csv_file(list_of_ids_of_familes_used_in_iuphar_papers,
                                                                        output_queries_from_papers)

execution.shutDown()