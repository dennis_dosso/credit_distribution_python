from modules import IUPHARDatabaseToolkit
from properties.properties import * # all the properties values


execution = IUPHARDatabaseToolkit()

# write the classes of queryes, one after the other
#the arguments are the paths
execution.write_all_queries_for_class_gpcr(output_gpcr_queries)
execution.write_all_queries_for_class_ion_channels(output_ion_queries)
execution.write_queries_for_enzimes(output_enzime_queries)
execution.write_queries_for_nhr(output_nhr_queries)
execution.write_queries_for_catalytic_receptors(output_catalytic_receptors_queries)
execution.write_queries_for_transporters(output_transporters_queries)
execution.write_queries_for_other_proteins(output_other_proteins_queries)

execution.shutDown()
print("done")