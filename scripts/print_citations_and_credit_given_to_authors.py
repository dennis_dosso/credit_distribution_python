from modules.rum import extract_column_from_csv_file
from modules import IUPHARDatabaseToolkit
from properties.properties import *
from datastructures.authors_map import AuthorsMap
from datastructures.citation_unit_map import CUMap
import pandas as pd

""" This script gathers all the queries used in the gathered paper
on IUPHAR and writes one csv file with information about citations and credit
given to the different authors"""



if __name__ == '__main__':
    # first, read all the ids of the families from the file containing the queries
    family_ids = extract_column_from_csv_file(list_of_ids_of_familes_used_in_iuphar_papers)
    family_ids = family_ids[0]

    execution = IUPHARDatabaseToolkit.IUPHARDatabaseToolkit()
    # map to keep the authors and their information
    authors_map = AuthorsMap()
    # map to keep information about the papers
    families_map = CUMap()

    families_without_authors = [] # just to know families that do not have authors
    total_number_of_citations = 0
    total_distributed_credit = 0

    # now that we have the ids, we can:
    for family_id in family_ids:
        # 1) compute the total quantity of credit that goes this query
        credit_of_this_query = execution.compute_total_credit_generated_from_family_query_with_id(family_id)
        total_distributed_credit += credit_of_this_query
        total_number_of_citations += 1

        # ----- FAMILY -----
        # get the name of the family
        family_name = execution.get_name_of_the_family_with_this_id(family_id)
        # add the name to the maps of families
        families_map.add_a_citation_unit(family_id)
        families_map.add_citation_to_the_unit(family_id)
        families_map.add_credit_to_unit(credit_of_this_query, family_id)
        families_map.add_name_to_citation_unit(family_id, family_name)


        # ----- AUTHORS -----
        #  compute the set of authors of this query
        (list_of_authors, id_list) = execution.get_all_the_autors_associated_to_the_family_with_this_id(family_id)
        number_of_authors = len(id_list)

        if number_of_authors == 0:
            if family_id not in families_without_authors:
                families_without_authors.append(family_id)
            continue

        # every author receives an equal quantity of credit
        portion_of_credit_going_to_one_author = credit_of_this_query / number_of_authors

        # add all the authors of this query to the authors seen
        authors_map.add_list_of_authors(id_list)
        counter = 0
        for author_id in id_list:
            authors_map.add_name_to_author(author_id, list_of_authors[counter])
            authors_map.add_citation_to_author(author_id)
            authors_map.add_credit_to_author(credit=portion_of_credit_going_to_one_author, author_id=author_id)
            counter = counter + 1

    total_number_of_queries = len(families_map.entities_map)
    # now we can print the information in a file for easy reference
    authors_map.print_content_in_a_csv_file(authors_map_output_file)
    families_map.print_content_in_a_csv_file(families_map_output_file)


    print('families without authors: ' + str(families_without_authors) +
          '\n' + str(len(families_without_authors)))
    print("impact factor of this 'journal': " + str(total_number_of_citations / total_number_of_queries))
    print("credit impact factor of this 'journal': " + str(total_distributed_credit / total_number_of_queries))

    execution.shutDown()
