import pandas as pd
import numpy as np
import scipy.stats as stat

from properties.properties import csv_input_file

# read the csv file
table = pd.read_csv(csv_input_file)
# isolate the column with citations
citation_counts = table[['how']].values
#transpose
citation_counts = np.atleast_2d(citation_counts).T
# remove header
citation_counts = citation_counts[0]
#find the highest value
citation_max = np.amax(citation_counts)
#normalize everything
citation_counts = citation_counts / citation_max

#do the same, this time with lineage
credits_ = table[['why']]
credits_ = credits_.values
credits_ = np.atleast_2d(credits_).T
credits_ = credits_[0]
credit_max = np.amax(credits_)
credits_ = credits_ / credit_max

linear_correlation = stat.pearsonr(credits_, citation_counts)
rank_correlation = stat.kendalltau(credits_, citation_counts)
spearman_correlation = stat.spearmanr(credits_, citation_counts)

print('linear correlation: ' + str(linear_correlation))
print('rank correlation: ' + str(rank_correlation))
print('spearman correlation: ' + str(spearman_correlation))