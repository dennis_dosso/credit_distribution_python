import psycopg2 as psy

#import data to connect to database
from properties.properties import *

#get query to obtain all families made by an UK authors
from queries.IUPHARqueries import sql_get_uk_families
from queries.IUPHARqueries import sql_get_tuple_id

from queries.iuphar_provenance_queries import get_family_uk_author

#open connection to database
connection = psy.connect(
            "host=" + host +
            " dbname=" + database_name +
            " user=" + user_name +
            " password=" + password)

#create cursor to database
cur = connection.cursor()


#perform query to obtain all the ids of the families with author from UK
cur.execute(sql_get_uk_families)
#empty list for the ids
id_list = []
for tuple in cur.fetchall():
    family_id = tuple[0]
    id_list.append(family_id)

#print(id_list)

#open file
f = open(output_how_oriented_queries, 'w')

#now we have the list of ids. We need to print the corresponding queries to perform the extraction of provenance
for id_ in id_list:
    #prepare the query taking it from the properties. Set the id
    building_query = get_family_uk_author % (id_, id_)
    f.write(building_query + "\n")

f.close()

f = open(ids_file, 'w')
f2 = open(tuple_ids_file, 'w')

for id_ in id_list:
    f.write(str(id_) + '\n')
    new_query = sql_get_tuple_id % (id_)
    cur.execute(new_query)
    for tuple in cur.fetchall():
        tuple_id = tuple[0]
        f2.write(str(tuple_id) + '\n')

f.flush()
f.close()
f2.flush()
f2.close()

cur.close()
connection.close()

