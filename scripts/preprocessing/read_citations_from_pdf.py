#given a directory containing PDF files, papers citing IUPHAR, this script retrieves the citations and
#prints a file with their counts

from modules.rum import get_all_files_in_directory
from modules.PDFToolkit import PDFToolkit

from properties.properties import pdf_directory
from properties.properties import family_output_file
from properties.properties import ligand_output_file
from properties.properties import object_output_file

IUPHAR_KEYWORD = 'guidetopharmacology'
FAMILY_KEYWORD = 'FamilyDisplayForward'  # families contain this keyword in their url
OBJECT_KEYWORD = 'ObjectDisplayForward'  # objects contain this keyword in their url
LIGAND_KEYWORD = 'LigandDisplayForward'  # ligands contain this keyword in their url


if __name__ == '__main__':
    print("reading the papers...")
    #reading all the papers in the directory
    pdf_list = get_all_files_in_directory(pdf_directory)

    family_file = open(family_output_file, "w")
    object_file = open(object_output_file, "w")
    ligand_file = open(ligand_output_file, "w")

    #to be concluded...