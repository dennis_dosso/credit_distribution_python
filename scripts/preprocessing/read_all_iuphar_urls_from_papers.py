from modules.rum import get_all_files_in_directory
from modules.PDFToolkit import PDFToolkit

# take the directory containing the PDFs
from properties.properties import pdf_directory
from properties.properties import family_output_file
from properties.properties import ligand_output_file
from properties.properties import object_output_file

"""A script to read all the urls in a webpage and write them in three different files"""

IUPHAR_KEYWORD = 'guidetopharmacology'
FAMILY_KEYWORD = 'FamilyDisplayForward'  # families contain this keyword in their url
OBJECT_KEYWORD = 'ObjectDisplayForward'  # objects contain this keyword in their url
LIGAND_KEYWORD = 'LigandDisplayForward'  # ligands contain this keyword in their url


def extract_id_from_url(url):
    """

    :param url:
    :return: the id of extracted and True if present, otherwise -1 and False
    if no id could be found
    """
    # we only need the id of this url
    parts = url.split('=')
    # no id here
    if len(parts) < 2:
        return -1, False
    # take the first part
    id = parts[1]
    id_parts = id.split('#')
    if len(id_parts) >= 2:
        id = id_parts[0]

    id_parts = id.split('&')
    if len(id_parts) >= 2:
        id = id_parts[0]
    return id, True


# take all the files inside this directory
pdf_list = get_all_files_in_directory(pdf_directory)

# open three different files
family_file = open(family_output_file, "w")
object_file = open(object_output_file, "w")
ligand_file = open(ligand_output_file, "w")

# extract all the urls from these pdfs
for pdf_path in pdf_list:
    # read all the urls in the PDF
    url_list = PDFToolkit.read_all_urls_in_pdf(pdf_path)
    if url_list is None:
        continue

    # now we have all the urls
    for url in url_list:
        if IUPHAR_KEYWORD in url:
            # take the id from the url
            (id, present) = extract_id_from_url(url)
            if not present:  # special case, a IUPHAR link without id (it should not be possible, but just in case)
                continue
            # we are interested in this url
            if FAMILY_KEYWORD in url:
                family_file.write(id + "\n")
            elif OBJECT_KEYWORD in url:
                object_file.write(id + "\n")
            elif LIGAND_KEYWORD in url:
                ligand_file.write(id + "\n")

# we read all PDF and all urls in these PDF, time to close everything
family_file.close()
object_file.close()
ligand_file.close()

print('done')
