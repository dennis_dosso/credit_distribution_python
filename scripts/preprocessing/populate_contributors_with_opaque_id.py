from random import randint
import psycopg2 as psy
from properties.properties import *

""" This script populates the column anonymous_id of the table contributor in
IUPHAR with ids that are randomly generated. In this way we uniquely identify authors
in a way that makes them anonymous"""

#returns all the original ids
SQL_GET_ALL_CONTRIBUTORS = 'select c.contributor_id from contributor c'

SQL_UPDATE_ANONYMOUS_ID = 'update contributor set anonymous_id = %i where contributor_id = %i'

random_id_list = []

#connect to database
connection = psy.connect(
            "host=" + host +
            " dbname=" + database_name +
            " user=" + user_name +
            " password=" + password)
cur = connection.cursor()
update_cur = connection.cursor()

cur.execute(SQL_GET_ALL_CONTRIBUTORS)


for tuple in cur.fetchall(): #for each contributor
    #get the id of the contributor
    id_ = tuple[0]
    #produce a random number
    random_id = randint(1, 5000)
    while random_id in random_id_list:
        #if we have already generated this number, try again
        random_id = randint(1, 5000)
    random_id_list.append(random_id)
    #prepare the update query
    sql_query = SQL_UPDATE_ANONYMOUS_ID % (random_id, id_)
    update_cur.execute(sql_query)

connection.commit()
connection.close()

